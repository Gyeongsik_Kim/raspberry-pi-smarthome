﻿import RPi.GPIO as GPIO

import time

LED21 = 21
LED20 = 20
LED19 = 19
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED21, GPIO.OUT)
GPIO.setup(LED20, GPIO.OUT)
GPIO.setup(LED19, GPIO.OUT)
LED21 = GPIO.PWM(LED21, 100)
LED20 = GPIO.PWM(LED20, 100)
LED19 = GPIO.PWM(LED19, 100)

LED21.start(0)
LED20.start(0)
LED19.start(0)

delay = 0.1

try:
	while True:
		LED21.ChangeDutyCycle(100)
		LED20.ChangeDutyCycle(100)
		LED19.ChangeDutyCycle(100)
		time.sleep(1)
		LED21.ChangeDutyCycle(0)
		LED20.ChangeDutyCycle(0)
		LED19.ChangeDutyCycle(0)
		time.sleep(1)
except KeyboardInterrupt:
	LED21.stop()
	LED20.stop()
	LED19.stop()
	GPIO.cleanup()

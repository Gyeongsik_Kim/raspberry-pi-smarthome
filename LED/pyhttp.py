from flask import Flask,redirect
from time import sleep
import RPi.GPIO as GPIO
import time

app = Flask(__name__)

@app.route("/myhome")
def hello():
	LED21 = 21
	LED20 = 20
	LED19 = 19
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(LED21, GPIO.OUT)
	GPIO.setup(LED20, GPIO.OUT)
	GPIO.setup(LED19, GPIO.OUT)
	LED21 = GPIO.PWM(LED21, 100)
	LED20 = GPIO.PWM(LED20, 100)
	LED19 = GPIO.PWM(LED19, 100)

	LED21.start(0)
	LED20.start(0)
	LED19.start(0)

        while True:
                LED21.ChangeDutyCycle(100)
                LED20.ChangeDutyCycle(100)
                LED19.ChangeDutyCycle(100)


        return redirect("http://192.168.0.32/wordpress/index.php",code=302)


if __name__=="__main__":
        app.run(host='0.0.0.0',port=8888, debug=True)

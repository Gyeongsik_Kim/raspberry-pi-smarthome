import RPi.GPIO as GPIO
import time
pin = 20
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, True)
time.sleep(5)
GPIO.output(pin, False)
GPIO.cleanup()

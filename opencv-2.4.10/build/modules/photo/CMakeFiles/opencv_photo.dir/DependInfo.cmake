# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/opencv-2.4.10/modules/photo/src/denoising.cpp" "/home/pi/opencv-2.4.10/build/modules/photo/CMakeFiles/opencv_photo.dir/src/denoising.cpp.o"
  "/home/pi/opencv-2.4.10/modules/photo/src/inpaint.cpp" "/home/pi/opencv-2.4.10/build/modules/photo/CMakeFiles/opencv_photo.dir/src/inpaint.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/pi/opencv-2.4.10/build/lib/libopencv_photo.so" "/home/pi/opencv-2.4.10/build/lib/libopencv_photo.so.2.4.10"
  "/home/pi/opencv-2.4.10/build/lib/libopencv_photo.so.2.4" "/home/pi/opencv-2.4.10/build/lib/libopencv_photo.so.2.4.10"
  )


# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pi/opencv-2.4.10/build/modules/core/CMakeFiles/opencv_core.dir/DependInfo.cmake"
  "/home/pi/opencv-2.4.10/build/modules/imgproc/CMakeFiles/opencv_imgproc.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../modules/photo/perf"
  "../modules/video/include"
  "../modules/features2d/include"
  "../modules/highgui/include"
  "../modules/imgproc/include"
  "../modules/flann/include"
  "../modules/core/include"
  "../modules/ts/include"
  "../modules/photo/include"
  "modules/photo"
  "../modules/photo/src"
  "../modules/photo/test"
  "."
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~-g+)bwxWjgu}/Z2OT4{?:Mz{FbOka=OxzajAQY+%T{4f&+*&z_=e.~+7~)?OnR;');
define('SECURE_AUTH_KEY',  '=?kp9t5-Wua9DdixWa|ki_l4Xo6=ST bq~}K01Uk+IP+^y#UU$XaG9n5HlN%`CP1');
define('LOGGED_IN_KEY',    '_gK`Lg/PA|_=G0lvf+WO(rmUxfE>r{w2[N iO+E-_;2cjSRxStL&KBxe3vS*Uhyg');
define('NONCE_KEY',        'bZd@$:!+jvZ,7xwd+IP2o{uVy.6SG@REEoX 1Z1rmiM4.Kb/gh8v*8XIN6tsksMM');
define('AUTH_SALT',        '@}p2]V6VF@7LSi@Ig+YyE~|BhT`:LsKn3n6qZ3cu}0$a.iwK#wFbMAd{M%XF>%9/');
define('SECURE_AUTH_SALT', 'MdW ld`/ec<&3^(?g[%K `xF&^d`7{U(]fmG8V`]+/(AdUMHSVaHVI&o_=]}=?MJ');
define('LOGGED_IN_SALT',   'STc2VRJ}{#,H/7mKdOtt>6+e^Yl|5j?-U t|[.+hsJ|tk`G*Cyg3t,NI[b,O]Y4-');
define('NONCE_SALT',       '*ih3(/`Nj!Kp:~d[u*ay} 3QUa4_7[mtvtb^]<xN@fZD/XVk!F=N/saKEH|7k2|@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
